package Lesson8.AddAndDelete;

/**
 * Created by Zu on 10.12.2017.
 */
public interface LinkedList<T> {

   void add(T value);

   void remove(int index) throws LinkedListIndexOutOfBoundsException;

}
