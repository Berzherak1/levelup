package Lesson8.AddAndDelete;

/**
 * Created by Zu on 10.12.2017.
 */
public class LinkedListImpl<T> implements LinkedList<T> {

    private Element head;
    int size;

    // Внутренний класс - любой модификатор доступа
    private class Element {
        Element next;
        Element prev;

        // Тип генерика берется из внешнего класса
        T value;

        Element(T value) {
            this.value = value;
        }
    }

    @Override
    public void add(T value) {
        Element element = new Element(value);
        if (head == null) {
            head = element;
        } else {
            Element current = head;
            while (current.next != null) {
                current = current.next;
            }
            current.next = element;
            element.prev = current;
        }
    }

    @Override
    public void remove(int index) throws LinkedListIndexOutOfBoundsException{
        if (index < 0 || index >= size) {
            throw new LinkedListIndexOutOfBoundsException("Invalid index " + index + " .Please me correct index = " + size); // было просто remove;
        }
        if (index == 0) {
            head = head.next;
        } else {
            int currentIndex = 0;
            Element current = head;
            while (currentIndex != index) {
                current = current.next;
                currentIndex++;
            }

            current.prev.next = current.next;
        }
        size--;
    }
}

