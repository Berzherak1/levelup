package Lesson8.Exception;

/**
 * Created by Tester on 11.12.2017.
 */
public class tryCatchFinally {

    public static void main(String[] args) {

        System.out.println(tryCatchFinally(1));
        System.out.println(tryCatchFinally(-1));
    }

    private static int tryCatchFinally(int i) {
        try {
            System.exit(1);
            if (i >= 0) {
                return 10;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            return 15;
        } finally {
            return 20;
        }
    }
}
