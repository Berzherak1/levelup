package Lesson8.Exception;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Zu on 10.12.2017.
 */
public class CheckedExceptionExample {

    public static void main(String[] args) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try{
        Date date = simpleDateFormat.parse("08.12.1117 21:33:34");
            System.out.println(date);
        }catch (ParseException e){
            e.printStackTrace();
        }finally {
            System.out.println("Finally");
        }
    }
}
