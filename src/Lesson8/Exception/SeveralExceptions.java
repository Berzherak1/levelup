package Lesson8.Exception;

/**
 * Created by Tester on 11.12.2017.
 */
public class SeveralExceptions {

    public static void main(String[] args) {

        try {
            Class.forName("").newInstance();
            // multi-catch - ловим одно из трех исключений
        } catch (InstantiationException | IllegalAccessException  e) { // можно одно из них вынести в другой catch
            e.printStackTrace();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }
    }
}
