package Lesson8.Homework8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Tester on 11.12.2017.
 */
public class InvalidExceptionExample {

    public void enterNumber() throws IOException, InvalidException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите числа");
        String number = bufferedReader.readLine();
        String[] massiv = number.split(" ");
        for (String massiv1 : massiv) {
            int element = Integer.parseInt(massiv1);
            System.out.print("Введенные числа: " + element + " ");

            if (element < 0) {
                System.out.println();
                throw new InvalidException(massiv1 + " меньше нуля, вводи корректное значение");

            }
        }
    }
}