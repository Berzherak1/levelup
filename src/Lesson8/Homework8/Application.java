package Lesson8.Homework8;

import java.io.IOException;

/**
 * Created by Tester on 11.12.2017.
 */
public class Application {

    public static void main(String[] args) throws IOException, InvalidException {

        try {
            InvalidExceptionExample InvalidExceptionExample = new InvalidExceptionExample();
            InvalidExceptionExample.enterNumber();
        } catch (InvalidException e) {
            e.printStackTrace();
        }
    }
}