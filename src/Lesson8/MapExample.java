package Lesson8;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Zu on 10.12.2017.
 */
public class MapExample {

    public static void main(String[] args){

        Map<String, Integer> map = new HashMap<>(); //LinkedHashMap - сохраняет порядок


        map.put("lesson5",27);
        map.put("lesson6",1);
        map.put("lesson8",8);

        for(String key: map.keySet()){
            System.out.println("key" + key + ", value = " + map.get(key));

        }
        System.out.println(map.get("lesson5"));
        System.out.println(map.get("lesson7"));

        for(Integer value: map.values()){
        System.out.println(value);
        }
    }
}
