package Lesson8;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Zu on 10.12.2017.
 */
public class SetExample {

    public static void main(String[] args) {

        List<String> list = new ArrayList<>();

        list.add("1");
        list.add("2");
        list.add("3");
        list.add("1");

        System.out.println(list.toString());
        Set<String> set = new HashSet<>(); //LinkedHashSet - сохраняет порядок
        set.addAll(list);
        System.out.println(set.toString());


    }
}
