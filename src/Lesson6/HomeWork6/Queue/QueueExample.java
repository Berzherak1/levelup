package Lesson6.HomeWork6.Queue;

/**
 * Created by Tester on 05.12.2017.
 */
public class QueueExample {

    public static void main(String[] args) {

        QueueImpl queueImpl = new QueueImpl(3);

        queueImpl.enqueue(1);
        queueImpl.enqueue(2);
        queueImpl.enqueue(3);
        queueImpl.enqueue(4);
        queueImpl.enqueue(5);
        queueImpl.enqueue(6);

        queueImpl.dequeue();
        queueImpl.dequeue();

        System.out.println("Элементы в череди: ");
        while (!queueImpl.isEmpty()) {
            int value = queueImpl.dequeue();
            System.out.print(value);
            System.out.print(" ");
        }

        System.out.println(" ");
        System.out.println("Элементы в массиве: ");
        for (int i : queueImpl.array) {
            System.out.print(i);
            System.out.print(" ");
        }
    }
}

