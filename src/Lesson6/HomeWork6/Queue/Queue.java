package Lesson6.HomeWork6.Queue;

/**
 * Created by Tester on 06.12.2017.
 */
public interface Queue {

    public void enqueue(int element);

    public int dequeue();

    public boolean isEmpty();
}