package Lesson6.HomeWork6.Queue;

/**
 * Created by Tester on 05.12.2017.
 */

public class QueueImpl implements Queue {

    int[] array;
    int Size;
    int realElementsCount;
    int start;
    int end;

    public QueueImpl(int Size) {
        this.Size = Size;
        array = new int[Size];
        end = -1;
        start = 0;
        realElementsCount = 0;
    }

    public void enqueue(int element) {

        if (realElementsCount == array.length) {
            int[] oldArray = array;
            array = new int[(int) (array.length * 1.5)];
            System.arraycopy(oldArray, 0, array, 0, oldArray.length);
        }
        array[++end] = element;
        realElementsCount++;
    }

    public int dequeue() {

        int temp = array[start++];
        realElementsCount--;
        return temp;
    }

    public boolean isEmpty() {
        return (realElementsCount == 0);
    }
}









