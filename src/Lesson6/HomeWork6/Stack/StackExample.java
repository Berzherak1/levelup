package Lesson6.HomeWork6.Stack;

/**
 * Created by Tester on 06.12.2017.
 */
public class StackExample {

    public static void main(String[] args) {

        StackImpl stackImpl = new StackImpl(3);

        stackImpl.push(1);
        stackImpl.push(2);
        stackImpl.push(3);
        stackImpl.push(4);
        stackImpl.push(5);
        stackImpl.push(6);
        stackImpl.pop();
        stackImpl.pop();
        stackImpl.pop();

        System.out.print("Элементы в cтеке: ");
        while (!stackImpl.isEmpty()) {
            int value = stackImpl.pop();
            System.out.print(value);
            System.out.print(" ");
        }
        System.out.println(" ");
        System.out.print("Элементы в массиве: ");
        for (int i : stackImpl.array) {
            System.out.print(i);
            System.out.print(" ");
        }
    }
}





