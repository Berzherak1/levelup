package Lesson6.HomeWork6.Stack;

/**
 * Created by Tester on 06.12.2017.
 */
public class StackImpl implements Stack {

    int[] array;
    int head;
    int Size;
    int realElementsCount;

    public StackImpl(int Size) {
        this.array = new int[Size];
        head = -1;
        this.Size = Size;
    }

    public void push(int element) {

        copyArray();
        array[++head] = element;
        array[realElementsCount] = element;
        realElementsCount++;
    }

    public int pop() {
        return array[head--];
    }

    public boolean isEmpty() {
        return head == -1;
    }

    public void copyArray() {
        if (realElementsCount == array.length) {
            int[] oldArray = array;
            array = new int[(int) (array.length * 1.5)];
            System.arraycopy(oldArray, 0, array, 0, oldArray.length);
        }
    }
}
