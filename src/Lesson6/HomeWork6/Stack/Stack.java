package Lesson6.HomeWork6.Stack;

/**
 * Created by Tester on 06.12.2017.
 */
public interface Stack {

    public void push(int element);

    public int pop();

    public boolean isEmpty();

}
