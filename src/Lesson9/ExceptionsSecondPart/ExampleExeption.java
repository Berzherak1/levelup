package Lesson9.ExceptionsSecondPart;

/**
 * Created by Tester on 10.01.2018.
 */
public class ExampleExeption extends RuntimeException {

    public ExampleExeption(String message) {
        super(message);
    }
}
