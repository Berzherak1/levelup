package Lesson9.ExceptionsSecondPart;

/**
 * Created by Tester on 10.01.2018.
 */
public class RunExceptionExample implements Runnable {


    // обязательно надо обернуть
    @Override
    public void run(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
