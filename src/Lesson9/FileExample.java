package Lesson9;

import java.io.*;

/**
 * Created by Tester on 11.01.2018.
 */
public class FileExample {

    public static void main(String[] args) {
// относительный путь до файла (проект не указывем)
        File file = new File("LevelUp.iml");
        // при new - всегда будет true
        System.out.println(file != null);

        // проверка реального существования файла
        System.out.println("File exists " + file.exists());

        File dir = new File("src");
        System.out.println("Is dir exist " + dir.exists());
        System.out.println("Is folder = " + dir.isDirectory());
        System.out.println("Is file = " + dir.isFile());

// 7 java       BufferedReader reader = null;
 /* - эта конструкция try with resources */       try ( /* вместо строк 7 java пишем*/ BufferedReader reader = new BufferedReader(new FileReader(file))){
// 7 java           reader = new BufferedReader(new FileReader(file));

            String str = null;
            char[] chars = new char[1024];

            while ((str = reader.readLine()) != null) {
                System.out.println(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
            // до 7 java
//        } finally {
//            if (reader != null) {
//                try {
//                    reader.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            }
        }
    }

