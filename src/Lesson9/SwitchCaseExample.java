package Lesson9;

import java.util.Scanner;

/**
 * Created by Tester on 11.01.2018.
 */
public class SwitchCaseExample {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int var = scanner.nextInt();

        switch (var){

            case 1:
                System.out.println("I love java");
                break;

            case 2:
                System.out.println("You love java");
                break;
            // - если ввести, 3 то будет default
            default:
                    System.out.println("All love java");
                    break;
        }
        System.out.println("After switch");
    }
}
