package Lesson1;
import java.util.Scanner;

/**
 * Created by Tester on 14.11.2017.
 */

//2. Напишите программу, которая выдают четное число или нет. Число указываете в коде. Решение вывести на экран.
//3. Для этих двух программ сделайте возможность пользователю вводить значения с клавиатуры. Используйте класс Scanner.

public class Task2 {

    public static void main(String[] args) {

        int a;

        Scanner number = new Scanner(System.in);
        System.out.println("Введите число a");
        a = number.nextInt();

        if (a % 2 == 0) {
            System.out.println("Число a, равное " + a + " - четное");
        } else {
            System.out.println("Число a, равное " + a + " - нечетное");
        }
    }
}
