package Lesson1;
import java.util.Scanner;

/**
 * Created by Tester on 14.11.2017.
 */

//  1. Напишите программу, которая решает линейное уравнение "- ax + b = 0". Коэффициенты а и b указываете в коде. Решение вывести на экран.
//  3. Для этих двух программ сделайте возможность пользователю вводить значения с клавиатуры. Используйте класс Scanner.

public class Task1 {

    public static void main(String args[]) {

        int a, b;
        double x;

        Scanner number = new Scanner(System.in);

        System.out.println("Введите число a");
        a = number.nextInt();
        System.out.println("Введите число b");
        b = number.nextInt();

        x = (double) b / a; // -a*x + b = 0 --> a*x = b --> x = b / a
        System.out.println("x равен " + x);
    }
}

