package Lesson5;

/**
 * Created by Tester on 29.11.2017.
 */
public class User {

    private String login;
    private String password;

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object obj) {

        //Допустим, user.equals(user)
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof User))
            return false;

        User databaseUser = (User) obj;
        return login.equals(databaseUser.login) &&
                password.equals(databaseUser.password);
    }


        @Override
        public int hashCode() {
            int result = 31;

            result = 31 * result + login.hashCode();
            result = 31 * result + password.hashCode();
            return result;
        }

    }

