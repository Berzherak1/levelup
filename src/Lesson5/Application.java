package Lesson5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Tester on 29.11.2017.
 */
public class Application {
    public static void main(String[] args) throws IOException {
        User[] users = new User[]{
                new User("user1", "user11"),
                new User("user2", "user22"),
                new User("user3", "user33")
        };

        Test[] test = new Test[]{
                new Test("user1", "user11"),
                new Test("user2", "user22"),
                new Test("user3", "user33")
        };


        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Enter login");
            String login = bufferedReader.readLine();
            System.out.println("Enter password");
            String password = bufferedReader.readLine();

            User user = new User(login, password);
            boolean isLogin = false;
            for (User user1 : users) {
                if (user.hashCode() == user1.hashCode()&& user.equals(user1)) {
                    isLogin = true;
                    break;
                }
            }
            if (isLogin) {
                System.out.println("Login succesful");
            } else {
                System.out.println("No such user");
            }
        }
    }
}
