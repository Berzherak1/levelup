package Lesson5;

/**
 * Created by Tester on 30.11.2017.
 */
public class Test {

    private String login;
    private String password;

    public Test(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
