package Lesson5.Homework5;
import java.util.Date;

/**
 * Created by Tester on 30.11.2017.
 */
public class User {

    int userId;
    String name;
    String lastName;
    String email;
    Date registrationDate;

    public User( int userId, String name, String lastName, String email, Date registrationDate){
        this.userId = userId;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.registrationDate = registrationDate;
    }
}
