package Lesson10;

import Lesson10.pattern.Human;
import Lesson10.pattern.Singleton;
import com.sun.org.apache.xpath.internal.SourceTree;

/**
 * Created by Tester on 19.01.2018.
 */
public class Application {

    public static void main(String[] args) {

        OuterClass outer = new OuterClass();
        OuterClass.InnerClass innerClass = outer.new InnerClass();
        OuterClass.InnerClass innerClassInVar = outer.innerClass;


        // - объекты идентичны (здесь мы не создаем новый объект, а вызываем один и тот же)
        Singleton first = Singleton.getInstance();
        Singleton second = Singleton.getInstance();
        System.out.println(first == second);

        // создание объекта вложенного класса
        OuterClass.NestedClass nestedClass = new OuterClass.NestedClass(); // тут уже есть объект класса nested

        //
        OuterClass.InnerClass anotherInnerClass = new OuterClass().new InnerClass(); // - а тут я создаю объект класса inner

       final Human.HumanBuilder builder = Human.createBuilder();

//       builder.setAge(3);
//       builder.setHeight(2);
//       builder.setWeight(4);

        // равноценно 3-м строкам выше (без getHuman)
        //System.out.println(builder.setWeight(234).setHeight(43).setAge(4).setName("ef").getHuman());

        // Можно записать как:

        builder.setName("rgr")
                .setAge(34)
                .setHeight(43)
                .setWeight(343);
        Human human = builder.getHuman();

        System.out.println(human);



    }

}
