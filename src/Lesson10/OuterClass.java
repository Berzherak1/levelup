package Lesson10;

/**
 * Created by Tester on 19.01.2018.
 */
public class OuterClass {

    private int var;
    private static int staticVar;
    InnerClass innerClass;

    public void method(){
        InnerClass innerClass = new InnerClass();
    }

    public class InnerClass {

        public InnerClass() {

            var = 23;
            staticVar = 199;
        }

        public OuterClass getOuther(){
            return new OuterClass();
        }
    }

    public OuterClass getOuther(){
        return new OuterClass();
    }

    // вложенный класс (если внутренний класс static - он всегда вложенный)
    // объект внутреннего класса имеет доступ ко всем полям внешнего класса (не взирая на модификатор доступа),
    // но, объект вложенного класса не может обращаться к нестатическим переменным (т.к имеет модификатор доступа static)
    public static class NestedClass{

        public void method() {
            staticVar = 23;

        }
    }
}
