package Lesson10.pattern;

/**
 * Created by Tester on 24.01.2018.
 */
public class Human {

    private double weight;
    private double height;
    private int age;
    private String name;
    private String sex;
    private String religion;
    private String lastname;

//    public Human(double weight, double height, int age, String name, String sex, String religion, String lastname) {
//        this.weight = weight;
//        this.height = height;
//        this.age = age;
//        this.name = name;
//        this.sex = sex;
//        this.religion = religion;
//        this.lastname = lastname;
//    }

    // при вызове метода создается объект Human и объект HumanBuilder
    public static HumanBuilder createBuilder() {
        return new Human().new HumanBuilder();
/* это равносильно:
        Human human = new Human();
        HumanBuilder builder = human.new HumanBuilder();
        return builder;*/
    }


    @Override
    public String toString() {
        return "Human{" +
                "weight=" + weight +
                ", height=" + height +
                ", age=" + age +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", religion='" + religion + '\'' +
                ", lastname='" + lastname + '\'' +
                '}';
    }

    public class HumanBuilder {

        private HumanBuilder() {
        }

        // Здесь обращаемся к полю weight внешнего класса
        public HumanBuilder setWeight(double weight) {
            Human.this.weight = weight;
            return this;
        }

        public HumanBuilder setHeight(double height) {
            Human.this.height = height;
            return this;
        }

        public HumanBuilder setAge(int age) {
            Human.this.age = age;
            return this;
        }

        public HumanBuilder setName(String name) {
            Human.this.name = name;
            return this;
        }

        public HumanBuilder setSex(String sex) {
            Human.this.sex = sex;
            return this;
        }

        public HumanBuilder setReligion(String religion) {
            Human.this.religion = religion;
            return this;
        }

        public HumanBuilder setLastname(String lastname) {
            Human.this.lastname = lastname;
            return this;
        }

        public Human getHuman(){
            return Human.this;
        }

    }
}

