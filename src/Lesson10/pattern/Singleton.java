package Lesson10.pattern;

/**
 * Created by Tester on 24.01.2018.
 */
public class Singleton {

    // Singleton - одиночка - шаблон проектирования (то, как правильно писать код),
    // существует только один объект нашего класса
    // запретить вызов конструктора - сделать его private, никто не сможет создать объект

    private Singleton(){}

    // Пять слов обозначает одно и то же
    // 1. Объект
    // 2. Ссылка
    // 3. Instance
    // 4. Reference
    // 5. Переменная


    // любая переменная static final пишется с заглавных букв
    private static final Singleton INSTANCE = new Singleton();

    public static Singleton getInstance(){
        return INSTANCE;

    }
}
