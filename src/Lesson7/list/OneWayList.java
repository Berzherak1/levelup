package Lesson7.list;

/**
 * Created by Tester on 07.12.2017.
 */
public class OneWayList<T> implements List<T> {

    private ListElement head;
    int size = 0;

    @Override
    public boolean add(T value) {

        //Добавляемый элемент списка (в конец сейчас)
        ListElement<T> element = new ListElement<>(value);
        if (head == null) {
            head = element;
        }else {

            ListElement current = head;
            while (current.next!= null) {
                current = current.next;
            }
            current.next = element;
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(int index) {
        //1.Проверить переданный индекс - вдруг он меньше 0 или больше, чем кол-во элементов
        //2. Найти элемент под этим индексом
        //3. Перенаправить ссылку с предыдущего на следую щий элемент
        if (isEmpty()|| index > size){

            System.out.println("Пусто");


            //for l

        }

        return false;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public String toString() {
        if (isEmpty()) return "[]";
        // [el,el, ..., el]
        StringBuilder sb = new StringBuilder("[");

        ListElement current = head;
        while (current != null){
            sb.append(String.valueOf(current.value));
            sb.append(", ");
            current = current.next;
        }
        sb.delete(sb.length()-2,sb.length());
        sb.append("]");

        return sb.toString();
    }
}