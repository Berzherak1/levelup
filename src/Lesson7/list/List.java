package Lesson7.list;

/**
 * Created by Tester on 07.12.2017.
 */
public interface List<T> {

    boolean add(T value);

    boolean remove(int index);

    boolean isEmpty();
}
