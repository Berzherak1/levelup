package Lesson7;

import Lesson7.list.List;
import Lesson7.list.OneWayList;

/**
 * Created by Tester on 07.12.2017.
 */
public class Application {

    public static void main(String[] args) {

        List<Integer> list = new OneWayList<>();

        list.remove(1);
        list.add(3);
        list.add(5);
        list.add(6);
        list.add(9);


        System.out.println(list);

    }
}
