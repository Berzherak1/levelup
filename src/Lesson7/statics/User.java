package Lesson7.statics;

/**
 * Created by Zu on 07.12.2017.
 */
public class User {

    String user;
    static String defaultPassword;

    public User(String user){
        this.user = user;
    }

    @Override
    public String toString() {
        return "User{" +
                "user='" + user + '\'' +
                "defPassword=" + defaultPassword +
                '}';
    }
}