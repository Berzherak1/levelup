package Lesson7.statics;

/**
 * Created by Zu on 07.12.2017.
 */
public class StaticExample {

    public static void main(String[] args) {

        User user1 = new User("login1");
        User.defaultPassword  = "defPassword1";

        User user2 = new User("login2");

        User.defaultPassword  = "defPassword2";

        System.out.println(user1);
        System.out.println(user2);

    }
}