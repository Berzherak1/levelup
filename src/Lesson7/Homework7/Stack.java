package Lesson7.Homework7;

import Lesson7.Homework7.Collection.Collection;

/**
 * Created by Tester on 08.12.2017.
 *
 * 3. Написать интерфейсы Stack и Queue, которые наследуются от интерфейса Collection.
 * В них будет по два метода: push и pop.
 * push(E value) - добавляет значение
 * pop() - удаляет значение в зависимости от структуры данных
 */

public interface Stack<E> extends Collection{

    void push(E value);
    int pop();
}
