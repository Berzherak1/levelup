package Lesson7.Homework7.Collection;

import java.util.ArrayList;

/**
 * Created by Tester on 12.12.2017.
 */
public class CollectionImpl<T> implements Collection<T>{

    private ArrayListElement head;
    int size = 0;

    @Override
    public boolean isEmpty() {
        System.out.println("Пусто1");
        return (head == null);

    }

    public boolean add(T value) {

        //Добавляемый элемент списка (в конец сейчас)
        ArrayListElement<T> element = new ArrayListElement<>(value);
        if (head == null) {
            head = element;
        }else {

            ArrayListElement current = head;
            while (current.next!= null) {
                current = current.next;
            }
            current.next = element;
        }
        size++;
        return true;
    }


    public void remove(int index) {

        if (index < 0 || index >= size) {
            return;
        }
        ArrayListElement current = head;
        ArrayListElement prev = null;
        int currentIndex = 0;
        while (currentIndex != index) {
            prev = current;
            current = current.next;
            currentIndex++;
        }

        if(current == null) // Элемент не найден
        {
            return;
        }
        if(current==head)
        {
            head=current.next;
        } else if(prev != null)
        {
            prev.next=current.next;
        }
        size--;

    }

    @Override
    public int getSize() {
        return size;
    }



    @Override
    public T[]toArray1( Collection<T> list) {
        //ElementsCollection customerNicknamesAuthorList = $$(By.xpath("//span[@class='chat__offer__nickName']"));
        //ArrayListElement<Integer> customerNicknamesAuthorArr = new ArrayListElement<Integer>();
        //list = new CollectionImpl<>();

        //int[] array = list.toArray(new int[list.getSize()]);


        //for (Object list : gfeg) {
        //customerNicknamesAuthorArr.add(((SelenideElement) list).getText());
        //String[] arrayCustomerNicknameAuthors = customerNicknamesAuthorArr.toArray(new String[customerNicknamesAuthorList.size()]);
        T[] array = (T[]) new Object[size];
        ArrayListElement current = head;
        int i = 0;
        while (current.next != null) {
            //array[i] = current.value;
        }
return null;
    }




    @Override
    public String toString() {
        if (isEmpty()) return "[]";
        // [el,el, ..., el]
        StringBuilder sb = new StringBuilder("[");

        ArrayListElement current = head;
        while (current != null){
            sb.append(String.valueOf(current.value));
            sb.append(", ");
            current = current.next;
        }
        sb.delete(sb.length()-2,sb.length());
        sb.append("]");

        return sb.toString();
    }
}



//    @Override
//    public void remove1(int index) {
//        if (index < 0 || index >= size) {
//            return;
//        }
//
//        ArrayListElement current = head;
//        ArrayListElement prev = null;
//        int currentIndex = 0;
//        while (currentIndex != index) {
//            prev = current;
//            current = current.next;
//            currentIndex++;
//        }
//
//        if(current == null) // Элемент не найден
//        {
//            return;
//        }
//        if(current==head)
//        {
//            head=current.next;
//        } else if(prev != null)
//        {
//            prev.next=current.next;
//        }
//        size--;
//    }