package Lesson7.Homework7.Collection;

/**
 * Created by Tester on 08.12.2017.
 *
 * 1. Написать интерфейс Collection.
 * В нем определить методы:
 * 1. Проверка, пустая ли коллекция
 * 2. Вернуть размер коллекции
 * 3. Преобразовать коллекцию в массив - T[] toArray();
 */
public interface Collection<T> {

    boolean isEmpty();
    int getSize();
    T[] toArray1(Collection<T> collection);
    boolean add(T value);
    void remove(int index);

}
