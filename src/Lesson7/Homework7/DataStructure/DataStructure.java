package Lesson7.Homework7.DataStructure;

import Lesson7.Homework7.Collection.Collection;

/**
 * Created by Tester on 08.12.2017.
 *
 * 2. Потом определить интерфейс DataStructure. Этот интерфейс наследуется от интерфейса Collection.
 В нем определить методы:
 1. Добавление элемента
 2. Удаление элемента по индексу.
 */

public interface DataStructure<E> extends Collection<E> {

    boolean add(E value);
    void remove(int index);



}
