package Lesson4.WhatIsIt;

import java.util.Date;

/**
 * Created by Tester on 27.11.2017.
 */
public class Animal {

    private int weight = 2;
    Date date;

    public Animal(int weight, Date date) {
        this.weight = weight;
        this.date = date;
    }

    public int getWeight() {
        return weight;
    }

    public int setWeight(int weight) {
        if (weight<0) {
            this.weight = 0;
        }else
        {this.weight = weight;}
        return weight;
    }
}
