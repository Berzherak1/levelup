package Lesson4.Task1;

/**
 * Created by Zu on 25.11.2017.
 */
public class PostComment {

   private User user;

   public User getUser() {
      return user;
   }

   public void setUser(User user) {
      this.user = user;
   }

   @Override
   public boolean equals(Object object) {
      if (this == object) return true;
      if (!(object instanceof User))
         return false;

      PostComment other = (PostComment) object;
      return user.equals(other.user);
   }

   @Override
   public int hashCode() {
      int result = 17;
      result = 31 * result + user.hashCode();
      return result;
   }

}
