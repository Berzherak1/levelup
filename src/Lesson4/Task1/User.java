package Lesson4.Task1;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Zu on 25.11.2017.
 */
public class User {

        private int userId;
        private String name;
        private String lastName;
        private String email;
        private Date registrationDate;

        public User(int userId, String name, String lastName, String email, Date registrationDate) {
                this.userId = userId;
                this.name = name;
                this.lastName = lastName;
                this.email = email;
                this.registrationDate = registrationDate;
        }

        @Override
        public boolean equals(Object object) {
                if (this == object) return true;
                if (!(object instanceof User))
                        return false;

                User other = (User) object;
                return userId == other.userId &&
                        lastName.equals(other.lastName) &&
                        email.equals(other.email) &&
                        registrationDate.equals(other.registrationDate);
        }

        @Override
        public int hashCode() {
                int result = 17;
                result = 31 * result + userId;
                result = 31 * result + lastName.hashCode();
                result = 31 * result + email.hashCode();
                result = 31 * result + registrationDate.hashCode();
                return result;
        }
}
