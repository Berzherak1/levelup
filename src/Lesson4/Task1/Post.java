package Lesson4.Task1;

import java.util.Date;

/**
 * Created by Zu on 25.11.2017.
 */
public class Post {

    private int postId;
    private String title;
    private Date postedAt;

    public Post(int postId, String title, Date postedAt) {
        this.postId = postId;
        this.title = title;
        this.postedAt = postedAt;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof Post))
            return false;

        Post other = (Post) object;
        return postId == other.postId &&
                title.equals(other.title) &&
                postedAt.equals(other.postedAt);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + postId;
        result = 31 * result + title.hashCode();
        result = 31 * result + postedAt.hashCode();
        return result;
    }
}