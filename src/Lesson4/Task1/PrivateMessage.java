package Lesson4.Task1;

/**
 * Created by Zu on 25.11.2017.
 */
public class PrivateMessage {
   private Post post;

   public Post getPost() {
      return post;
   }

   public void setPost(Post post) {
      this.post = post;
   }

   @Override
   public boolean equals(Object object) {
      if (this == object) return true;
      if (!(object instanceof PrivateMessage))
         return false;

      PrivateMessage other = (PrivateMessage) object;
      return post.equals(other.post);
   }

   @Override
   public int hashCode() {
      int result = 17;
      result = 31 * result + post.hashCode();
      return result;
   }
}
